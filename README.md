
# EMDD-Mitos API Client

Client to consume the EMDD-Mitos API. The api is located at `https://api.mitos.gov.gr`.

## Build the package

We use poetry. 

```
python3 -m venv venv
source venv/bin/activate
pip install poetry
poetry install
poetry run examples/example.py
poetry build
```
