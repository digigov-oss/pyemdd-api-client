#!/usr/bin/env python3

import logging
import argparse
import json
from pyemdd import EmddClient

logger = logging.getLogger(__name__)

def main(args):
    level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=level)

    client = EmddClient()

    logger.info("Get process")
    process = client.get_process(285594)
    logger.info("Process = {}".format(json.dumps(process, ensure_ascii=False, indent=4)))

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Example")
    parser.add_argument(
        "--debug",
        metavar="FLAG",
        type=bool,
        action=argparse.BooleanOptionalAction,
        dest="debug",
        default=False,
        help="Show debug logs",
    )
    args = parser.parse_args()
    main(args)
